### docker

```
ILP CI-CD project - dockerization
```
---

### Dockerfile

#### To create docker image
```
Pre-requisite: Copy the java archive file, tomcat archive file and webapp archive file in the Dockerfile location

docker build -t <image_tag_name> -f Dockerfile .
```
#### To Run/Start the docker container with the image created in previous step
```
docker run -d -p <expose_port>:8080 --name <container_name> <image_tag_name>

***Check the webapp service in browser***
```
##### To Check the running container status
```
docker ps
```
#### To stop the running container
```
docker container stop <container_name>
```
---

### docker-compose

#### To Run/Deploy service using docker-compose.yaml
```
docker-compose up

***Check the webapp service in browser***
```
#### To Deploy service in docker swarm using docker-compose-deploy.yaml - Deploy in 3 nodes as replicas defined in yaml file
```
docker swarm init
docker stack deploy -c docker-compose-deploy.yaml <service_name>

***Check the webapp service in browser***
```
##### To Check the docker service status
```
docker service ls
docker service ps <service_name>
```
##### To Check the docker network status
```
docker network ls
```
#### To Stop the deployed service
```
docker stack rm <service_name>
```
#### To leave the docker swarm manager
```
docker swarm leave --force
```
---
