FROM ubuntu:16.04
WORKDIR /opt/java
ADD openjdk-11.0.2_linux-x64_bin.tar .
ENV JAVA_HOME /opt/java/jdk-11.0.2
ENV PATH $PATH:$JAVA_HOME/bin
WORKDIR /opt/tomcat
ADD apache-tomcat-9.0.37.tar .
COPY ILP_BookStore.war /opt/tomcat/apache-tomcat-9.0.37/webapps
EXPOSE 8080
CMD ["/opt/tomcat/apache-tomcat-9.0.37/bin/catalina.sh", "run"]
